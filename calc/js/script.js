/*jslint plusplus: true */
'use strict';
if (document.getElementById('history').children[1] !== null) {
  var HISTORY;
  HISTORY = document.getElementById('history').children[1];
}
if (document.getElementById('summary').children[0] !== null) {
  var SUMMARY;
  SUMMARY = document.getElementById('summary').children[0];
}

/*запись в историю и в саммери до первого оператора*/
var arr, arrHistory;
arr = [];
arrHistory = [];

function pullToHistory(value) {
  if (arrHistory.indexOf('<img src="images/assets/icon_division@2x.png">') > -1
      || arrHistory.indexOf('<img src="images/assets/icon_multiplication@2x.png">') > -1
      || arrHistory.indexOf('<img src="images/assets/icon_plus@2x.png">') > -1
      || arrHistory.indexOf('<img src="images/assets/icon_minus@2x.png">') > -1) {
    arrHistory.push(value);
    HISTORY.innerHTML = arrHistory.join('');
  } else {
    arrHistory.push(value);
    arr = arrHistory.concat();
    HISTORY.innerHTML = arrHistory.join('');
    SUMMARY.innerHTML = arr.join('');
  }
}

/*арифметические операции и вывод в саммери*/
var valueL, valueR, str;
valueL = '';
valueR = '';
str = '';

function result(value) {
  var i, j;
  str += value; //пишем всю историю встроку
  //считаем сколько в строке операторов
  for (i = 0; i < str.length; i++) {
    if (str.indexOf('+') === i) {
      j = '' + i;
    }
  }
//  alert(j);
  if (value === '+') {
    valueL = +valueL + +valueR;
    SUMMARY.innerHTML = valueL;
    valueR = '';
    return;
  }
  if (value === '-') {
    valueL = valueL - valueR;
    SUMMARY.innerHTML = valueL;
    valueL = '';
    return;
  }
  if (str.indexOf('+') === -1
      || str.indexOf('-') === -1
      || str.indexOf('*') === -1
      || str.indexOf('/') === -1) {
    valueR += value;
  }
}

/* числа */
document.getElementById('zero').onclick = function () {
  if (arrHistory[0] === 0 && arrHistory.length === 1
      || arrHistory[arrHistory.length - 1] === 0
      && (arrHistory.indexOf('<img src="images/assets/icon_division@2x.png">') > -1
        || arrHistory.indexOf('<img src="images/assets/icon_multiplication@2x.png">') > -1
        || arrHistory.indexOf('<img src="images/assets/icon_plus@2x.png">') > -1
        || arrHistory.indexOf('<img src="images/assets/icon_minus@2x.png">') > -1)) {
    return;
  } else {
    pullToHistory(0);
  }
  result('0');
};

document.getElementById('one').onclick = function () {
  pullToHistory(1);
  result('1');
};

document.getElementById('two').onclick = function () {
  pullToHistory(2);
  result('2');
};

document.getElementById('three').onclick = function () {
  pullToHistory(3);
  result('3');
};

document.getElementById('four').onclick = function () {
  pullToHistory(4);
  result('4');
};

document.getElementById('five').onclick = function () {
  pullToHistory(5);
  result('5');
};

document.getElementById('six').onclick = function () {
  pullToHistory(6);
  result('6');
};

document.getElementById('seven').onclick = function () {
  pullToHistory(7);
  result('7');
};

document.getElementById('eight').onclick = function () {
  pullToHistory(8);
  result('8');
};

document.getElementById('nine').onclick = function () {
  pullToHistory(9);
  result('9');
};
/* числа конец */

//lastIndexOf не ищет с конца, поэтому нужно перебирать циклом
document.getElementById('comma').onclick = function () {
  //если стоит равно, то уже нельзя поставить минус
  if (arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_equality@2x.png">') {
    return;
  }
  
  if (arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_division@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_multiplication@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_plus@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_minus@2x.png">'
      || arrHistory[arrHistory.length - 1] === '-') {
    pullToHistory('0');
    pullToHistory(',');
  }
  
  var i;
  for (i = arrHistory.length - 1; i >= 0; i--) {
    if (arrHistory[i] === '<img src="images/assets/icon_division@2x.png">'
        || arrHistory[i] === '<img src="images/assets/icon_multiplication@2x.png">'
        || arrHistory[i] === '<img src="images/assets/icon_plus@2x.png">'
        || arrHistory[i] === '<img src="images/assets/icon_minus@2x.png">') {
      if (arrHistory.indexOf(',', i) > -1) {
        return;
      } else {
        pullToHistory(',');
      }
    }
  }
  
  if (arrHistory.indexOf(',') > -1) {
    return;
  } else {
    if (arrHistory.length === 0) {
      arrHistory.push(0);
      arrHistory.push(',');
      arr = arrHistory.concat();
      HISTORY.innerHTML = arrHistory.join('');
      SUMMARY.innerHTML = arr.join('');
    } else {
      pullToHistory(',');
    }
  }
};

document.getElementById('plus-minus').onclick = function () {
  //если стоит равно, то уже нельзя поставить минус
  if (arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_equality@2x.png">') {
    return;
  }
  
  var i;
  for (i = arrHistory.length - 1; i >= 0; i--) {
    //удаление после последнего арифметического оператора
    if (arrHistory[i] === '-'
        && (arrHistory.indexOf('<img src="images/assets/icon_division@2x.png">') > -1
        || arrHistory.indexOf('<img src="images/assets/icon_multiplication@2x.png">') > -1
        || arrHistory.indexOf('<img src="images/assets/icon_plus@2x.png">') > -1
        || arrHistory.indexOf('<img src="images/assets/icon_minus@2x.png">') > -1)) {
      arrHistory.splice(i, 1);
      HISTORY.innerHTML = arrHistory.join('');
      return;
    }
    //добавление после последнего арифметического оператора
    if (arrHistory[i] === '<img src="images/assets/icon_division@2x.png">'
        || arrHistory[i] === '<img src="images/assets/icon_multiplication@2x.png">'
        || arrHistory[i] === '<img src="images/assets/icon_plus@2x.png">'
        || arrHistory[i] === '<img src="images/assets/icon_minus@2x.png">') {
      arrHistory.splice(i + 1, 0, '-');
      HISTORY.innerHTML = arrHistory.join('');
      return;
    }
  }
  
  //для самого первого минуса добавление и удаление
  if (arrHistory.indexOf('-') === 0) {
    arrHistory.shift();
    arr = arrHistory.concat();
    HISTORY.innerHTML = arrHistory.join('');
    SUMMARY.innerHTML = arr.join('');
  } else {
    arrHistory.unshift('-');
    arr = arrHistory.concat();
    HISTORY.innerHTML = arrHistory.join('');
    SUMMARY.innerHTML = arr.join('');
  }
};

document.getElementById('clear').onclick = function () {
  arrHistory.length = 0;
  arr.length = 0;
  HISTORY.innerHTML = '';
  SUMMARY.innerHTML = '';
  valueL = '';
  valueR = '';
  str = '';
};

document.getElementById('backspace').onclick = function () {
  //если стоит равно, то уже нельзя поставить минус
  if (arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_equality@2x.png">') {
    return;
  }

  if (arrHistory.indexOf('<img src="images/assets/icon_division@2x.png">') > -1
      || arrHistory.indexOf('<img src="images/assets/icon_multiplication@2x.png">') > -1
      || arrHistory.indexOf('<img src="images/assets/icon_plus@2x.png">') > -1
      || arrHistory.indexOf('<img src="images/assets/icon_minus@2x.png">') > -1) {
    arrHistory.pop();
    HISTORY.innerHTML = arrHistory.join('');
  } else {
    arrHistory.pop();
    arr.pop();
    HISTORY.innerHTML = arrHistory.join('');
    SUMMARY.innerHTML = arr.join('');
  }
};

document.getElementById('divide').onclick = function () {
  if (arrHistory.length < 1
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_equality@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_division@2x.png">'
      || arrHistory[arrHistory.length - 1] === '-') {
    return;
  }
  if (arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_multiplication@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_plus@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_minus@2x.png">') {
    arrHistory[arrHistory.length - 1] = '<img src="images/assets/icon_division@2x.png">';
    HISTORY.innerHTML = arrHistory.join('');
  } else {
    arrHistory.push('<img src="images/assets/icon_division@2x.png">');
    HISTORY.innerHTML = arrHistory.join('');
    result('/');
  }
};

document.getElementById('multiply').onclick = function () {
  if (arrHistory.length < 1
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_multiplication@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_equality@2x.png">'
      || arrHistory[arrHistory.length - 1] === '-') {
    return;
  }
  if (arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_division@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_plus@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_minus@2x.png">') {
    arrHistory[arrHistory.length - 1] = '<img src="images/assets/icon_multiplication@2x.png">';
    HISTORY.innerHTML = arrHistory.join('');
  } else {
    arrHistory.push('<img src="images/assets/icon_multiplication@2x.png">');
    HISTORY.innerHTML = arrHistory.join('');
    result('*');
  }
};

document.getElementById('minus').onclick = function () {
  if (arrHistory.length < 1
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_minus@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_equality@2x.png">'
      || arrHistory[arrHistory.length - 1] === '-') {
    return;
  }
  if (arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_division@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_multiplication@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_plus@2x.png">') {
    arrHistory[arrHistory.length - 1] = '<img src="images/assets/icon_minus@2x.png">';
    HISTORY.innerHTML = arrHistory.join('');
  } else {
    arrHistory.push('<img src="images/assets/icon_minus@2x.png">');
    HISTORY.innerHTML = arrHistory.join('');
    result('-');
  }
};

document.getElementById('plus').onclick = function () {
  if (arrHistory.length < 1
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_equality@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_plus@2x.png">'
      || arrHistory[arrHistory.length - 1] === '-') {
    return;
  }
  if (arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_division@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_multiplication@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_minus@2x.png">') {
    arrHistory[arrHistory.length - 1] = '<img src="images/assets/icon_plus@2x.png">';
    HISTORY.innerHTML = arrHistory.join('');
  } else {
    arrHistory.push('<img src="images/assets/icon_plus@2x.png">');
    HISTORY.innerHTML = arrHistory.join('');
    result('+');
  }
};

document.getElementById('equality').onclick = function () {
  if (arrHistory.length < 1) {
    return;
  }
  if (arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_division@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_multiplication@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_plus@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_minus@2x.png">'
      || arrHistory[arrHistory.length - 1] === '<img src="images/assets/icon_equality@2x.png">'
      || arrHistory[arrHistory.length - 1] === '-') {
    return;
  } else {
    arrHistory.push('<img src="images/assets/icon_equality@2x.png">');
    HISTORY.innerHTML = arrHistory.join('');
  }
};